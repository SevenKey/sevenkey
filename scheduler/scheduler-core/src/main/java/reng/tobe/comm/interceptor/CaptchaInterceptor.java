package reng.tobe.comm.interceptor;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;

import reng.tobe.comm.render.CaptchaRender;
import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.url.Interceptor;
import net.vsame.url2sql.url.impl.UrlMapping;
import net.vsame.url2sql.url.impl.UrlMapping.Chain;

public class CaptchaInterceptor implements Interceptor{
	
	private static Set<String> set = new HashSet<String>(); 

	static {
		set.add("/comm/sendSmsCode");
		set.add("/account/checkExist");
	}

	@Override
	public void init(UrlMapping urlMapping, ServletContext servletContext) {
	}

	@Override
	public void invoke(Chain chain) {
		Url2SqlContext context = WebHelper.getContext();
		if(!set.contains(context.getRequestUrl())){
			chain.next();
			return ;
		}
		
		if(CaptchaRender.isCodeOK()) {
			chain.next();
			return;
		}
		context.putError(9, "验证码错误");
	}

	@Override
	public void destroy() {
		
	}

}

package com.popreal.shop.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateHelper {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");
	
	/**
	 * 转换为 yyyy-MM-dd HH:mm:ss
	 * @param date
	 * @return
	 */
	public static String toString(Date date) {
		return sdf.format(date);
	}
	
	/**
	 * 将 yyyy-MM-dd HH:mm:ss转换为Date
	 * @param source
	 * @return
	 */
	public static Date toDate(String source){
		try {
			return sdf.parse(source);
		} catch (Exception e) {
		}
		return null;
	}
	
	
	public static String yyyyMMddHHmmss2String(Date date) {
		return yyyyMMddHHmmss.format(date);
	}
	public static Date yyyyMMddHHmmsstoDate(String source) {
		try {
			return yyyyMMddHHmmss.parse(source);
		} catch (Exception e) {
		}
		return null;
	}
	
	public static String toString(Date date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);;
		return sdf.format(date);
	}
	
	public static Date toDate(String source, String format){
		SimpleDateFormat sdf = new SimpleDateFormat(format);;
		try {
			return sdf.parse(source);
		} catch (Exception e) {
		}
		return null;
	}
	
	/**
	 * 计算基准时间   若干天后时间
	 * @param date 基准时间
	 * @param day 距离天数
	 * @return
	 */
	public static Date afterDay(Date date, int day) {
		//now 去时分秒
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.get(Calendar.HOUR_OF_DAY);
		if(calendar.get(Calendar.HOUR_OF_DAY) <= 5) {
			//业务原因, 4点往后用户可能开始创建作用, 当天不算
			day = day - 1;//可能是定时器
		}
		
		calendar.add(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public static class BetweenBean {
		int day, hour, minute;

		public int getDay() {
			return day;
		}
		public int getHour() {
			return hour;
		}
		public int getMinute() {
			return minute;
		}
		@Override
		public String toString() {
			return "BetweenBean [day=" + day + ", hour=" + hour + ", minute="
					+ minute + "]";
		}
	}
	
	/**
	 * 计算两个时间 天数
	 * @param start
	 * @param end
	 * @return
	 */
	public static BetweenBean betweenDateCount(Date start, Date end){
		BetweenBean bean = new BetweenBean();
		long minutes = ((end.getTime() - start.getTime())/1000/60);
		bean.minute = (int) (minutes % 60);
		int hours = (int) (minutes / 60);
		bean.hour = hours % 60;
		bean.day = hours / 24;
		return bean;
	}
	
	public static List<String> getBetweenDateStr(Date beginDate, Date endDate, String format){
		Calendar beginCalendar = Calendar.getInstance();
		beginCalendar.setTime(beginDate);
		Calendar endCalendar = Calendar.getInstance();
		endCalendar.setTime(endDate);
		List<String> dateList = new ArrayList<String>();
		while(beginCalendar.compareTo(endCalendar) < 0){
			dateList.add(toString(beginCalendar.getTime(), format));
			beginCalendar.add(Calendar.DAY_OF_YEAR, 1);
		}
		dateList.add(toString(endCalendar.getTime(), format));
		return dateList;
	}

}

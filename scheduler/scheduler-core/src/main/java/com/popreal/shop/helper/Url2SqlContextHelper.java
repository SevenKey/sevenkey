package com.popreal.shop.helper;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;

public class Url2SqlContextHelper {
	
	/***
	 * 临时方法，将来框架支持
	 * @param prefix
	 * @param m
	 */
	public static void putParam(String prefix, @SuppressWarnings("rawtypes") Map m) {
		Url2SqlContext context = WebHelper.getContext();
		for(Object key : m.keySet()) {
			String str = null; 
			Object o = m.get(key);
			if(o != null) {
				str = o.toString();
			}
			context.putParam(prefix + "." + key, str);
		}
	}
	
	/**
	 * 移除指定前缀开始参数
	 * @param prefix
	 */
	public static void removeParam(String prefix) {
		Url2SqlContext context = WebHelper.getContext();
		Map<String, String[]> map = context.getParams();
		
		Set<String> keys = new HashSet<String>(map.keySet());
		for(String key : keys) {
			if(key.startsWith(prefix)) {
				map.remove(key);
			}
		}
	}

}

$SDC.route('', function(app) {
	
	// 基于准备好的dom，初始化echarts实例
    // 指定图表的配置项和数据
   var option = {
	    tooltip : {
	        trigger: 'axis'
	    },
	    legend: {
	        data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
	    },
	    xAxis : [
	        {
	            type : 'category',
	            boundaryGap : false,
	            min: "dataMin",
	            data : ['周一','周二','周三','周四','周五','周六','周日']
	        }
	    ],
	    yAxis : [
	        {
	            type : 'value'
	        }
	    ],
	    series : [
	        {
	            name:'邮件营销',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[120, 132, 101, 134, 90, 230, 210]
	        },
	        {
	            name:'联盟广告',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[220, 182, 191, 234, 290, 330, 310]
	        },
	        {
	            name:'视频广告',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[150, 232, 201, 154, 190, 330, 410]
	        },
	        {
	            name:'直接访问',
	            type:'line',
	            stack: '总量',
	            areaStyle: {normal: {}},
	            data:[320, 332, 301, 334, 390, 330, 320]
	        },
	        {
	            name:'搜索引擎',
	            type:'line',
	            stack: '总量',
	            label: {
	                normal: {
	                    show: true,
	                    position: 'top'
	                }
	            },
	            areaStyle: {normal: {}},
	            data:[820, 932, 901, 934, 1290, 1330, 1320]
	        }
	    ]
	};
	
	var charts = function(id, option){
		setTimeout(function(){
	        var myChart = echarts.init(document.getElementById(id));
	        myChart.setOption(option);
		}, 500);			
	}
	
	var getSeriesConfig = function(name){
		var config = {
            name: name,
            type:'line',
            stack: '总量',
            areaStyle: {normal: {}},
            data:[]
       };
       return config;
	}
	
	var getArray = function(datas, keys, option){
		var retDatas = [];
		for(var i=0; i<keys.length; i++) {
			retDatas.push(getSeriesConfig(keys[i].title));
		}
		
		var xAxis = [];
		for(var i=0; i<datas.length; i++) {
			var d = datas[i];
			for(var temp=0; temp<retDatas.length; temp++) {
				var k = keys[temp].key;
				if(!d[k]) {
					d[k] = 0;
				}
				retDatas[temp].data.push(d[k]);
			}
			xAxis.push(d.date);
		}
		
		option.series = retDatas;
		option.xAxis[0].data = xAxis
	};
	var getDate = function(str, defaultVal) {
		if(str) {
			return moment(str);
		}
		return defaultVal;
	}
	
    app.action(["/", "/statistics/home/:day", "/statistics/home/:startDate/:endDate", ], "index.html", function($scope, $route, _API, $location, $timeout, $rootScope){
		var formart = function (date) {
			return date.format('YYYY-MM-DD');
        }
    	var dateObj = {
	        startDate: getDate($route.current.params.startDate, moment().subtract(6, 'days')),
	        endDate: getDate($route.current.params.endDate, moment())
    	};
    	
	   $('#daterange-btn').daterangepicker({
	          ranges: {
	            '近一周': [moment().subtract(6, 'days'), moment()],
	            '近一周(不包含今天)': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
	            '近两周(不包含今天)': [moment().subtract(14, 'days'), moment().subtract(1, 'days')],
	            '近三周(不包含今天)': [moment().subtract(21, 'days'), moment().subtract(1, 'days')],
	            '近一月(不包含今天)': [moment().subtract(29, 'days'), moment().subtract(1, 'days')],
	            '这一月': [moment().startOf('month'), moment().endOf('month')],
	            '上一月': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
	            '2016 年': [moment('2016-01-01'), moment('2016-12-31')]
	          },
	          format: 'YYYY-MM-DD',
	          startDate: dateObj.startDate,
	          endDate: dateObj.endDate
	        },
	        function (start, end) {
	        	$('#daterange-span').html(formart(start) + ' - ' + formart(end));
	        	var path = '/statistics/home/' + start.format('YYYY-MM-DD') + '/' + end.format('YYYY-MM-DD');
	        	$location.path(path);
	        	$scope.$apply();
	        }
	    );
	    $('#daterange-span').html(formart(dateObj.startDate) + ' - ' + formart(dateObj.endDate));
	    
	    var defaultTip = function(ps){
			var sb = [];
			sb.push(ps[0].name + "<br/>");
			if(ps.length >= 2) {
				sb.push(ps[0].seriesName + " 占比：" + (ps[0].value*100/(ps[0].value + ps[1].value)).toFixed(2) + "%");
				sb.push("<hr/>")
			}
			for(var i=0; i<ps.length; i++) {
				sb.push(ps[i].seriesName + ": ");
				sb.push(parseFloat(ps[i].value.toFixed(2)) + '<br/>');
			}
			return sb.join('');
		};
		
		var comm = function(id, data, configs, tip){
			var c = $.extend(true, {}, option);
			c.legend.data = [];
			for(var i=0; i<configs.length; i++) {
				c.legend.data.push(configs[i].title);
			}
			
			getArray(data, configs, c);
			if(!tip) {
				tip = defaultTip;
			}
			c.tooltip.formatter = tip;
	        charts(id, c);
		}
		
		

    });
	
});


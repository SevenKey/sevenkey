$SDC.ProductStyleKeys = ['Material', 'Style', 'Comfortable Season', 'Thickness Index', 'Fabric Softness Index', 
	'Fabric Elasticity Index', 'Sleeve', 'Bottom'
];

$SDC.ProductStyleDatas = {
	'Material': [
		'Cotton', 'Lycra', 'Lace', 'Chiffon', 'Linen', 'Silk', 'Knit', 'Wool', 'Leather', 'Blends', 'Velvet', 
		'Satin', 'Organza', 'Brocade', 'Polyester', 'Modal', 'Denim'
	],
	'Style': [
		'Casual', 'Formal', 'Sports', 'Vintage', 'Street', 'Bohemia', 'Gothic', 'Preppy'
	],
	'Comfortable Season': [
		'Spring', 'Summer', 'Autumn', 'Winter'
	],
	'Thickness Index': [
		'Ultra-Thin', 'Thin', 'Moderate', 'Thick', 'Ultra Thick'
	],
	'Fabric Softness Index': [
		'Super Soft', 'Soft', 'Moderate', 'Tough', 'Super Tough'
	],
	'Fabric Elasticity Index': [
		'Inelastic', 'Elastic'
	],
	'Sleeve': [
		'Long Sleeve', 'Short Sleeve', 'Half Sleeve', 'Seven-Tenths Sleeve', 'Sleeveless'
	],
	'Bottom': [
		'Long Pants', 'Ninth Pants', 'Seven Pants', 'Shorts'
	]
};





$SDC.route('people/', function(app) {

	app.action(['/people/page/:page', '/people/search/:searchWord/page/:page'], 'peoplePage.html', ['$scope', '$route', '_API', '$location', '$timeout', '$rootScope',
		function($scope, $route, _API, $location, $timeout, $rootScope) {
			var type = $route.current.params.type;
			var page = $route.current.params.page;
			$scope.searchWord = $route.current.params.searchWord;
			$scope.pageUrl = '/people/page/';
			
			var reload = function(){
				console.log(_USER.peopleInfo.role);
				_API.comm('manage/people/page', {
					role : type,
					currentpage : page,
					pagesize : 20
				}).success(function(json){
					$scope.pv = json.pv;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			var search = function(){
				_API.comm('manage/people/search', {
					word : $scope.searchWord,
					currentpage : 1,
					pagesize : 20
				}).success(function(json){
					$scope.pv = json.pv;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			if($scope.searchWord){
				search();
				$scope.pageUrl = '/people/search/'+ $scope.searchWord + '/page/';
			}else{
				reload();
			}
			
			$scope.changePeopleRole = function(people, role, msg){
				if(!confirm(msg)){
					return;
				}
				_API.comm('manage/people/changeRole', {
					peopleId : people.id,
					role : role
				}).success(function(json){
					people.role = role;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			$scope.search = function(word){
				$location.path('/people/search/' + word + '/page/1');
			}
  
		}
	]);
	
	app.action(['/people/add', '/people/edit/:id'], 'peopleEdit.html', ['$scope', '$route', '_API', '$location', '$timeout', '$rootScope',
		function($scope, $route, _API, $location, $timeout, $rootScope) {
			
			var _id = $route.current.params.id;
			if(_id){
				$scope.title = "修改用户";
			}else{
				$scope.title = "新增用户";
			}
			$scope.m = {items : [{}]};
			
			$scope.save = function(m){
				var url = 'manage/people/add';
			if(_id){
				url = 'manage/people/update';
			}
			console.log(m.items);
			m.items = angular.toJson(m.items);
			_API.comm(url, m).success(function(json){
				
				alert("操作成功");
			$location.path('/people/page/1');
				}).error(function(json){
					alert(json.msg);
				});
			};
		
			//加载所有的分组
			_API.comm('manage/group/all', {
			}).success(function(json){
				$scope.groups = json.datas;
				
	    	});
			
			if(_id){
				_API.comm('manage/people/getById', {
						id : _id
					}).success(function(json){
						$scope.m = json.data;
					}).error(function(json){
						alert(json.msg);
					});
				}
		
		}]);
	
});
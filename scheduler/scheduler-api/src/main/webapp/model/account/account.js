(function(app){
	
	app.action(['/login'], 'account/login.html', 
		['$scope', '$route', '_API', '$location', '$timeout', '$rootScope', 
		function($scope, $route, _API, $location, $timeout, $rootScope) {
		
		$scope.isAutoLogin = true;
		
		$scope.m = {
			loginname: "",
			password: ""
		}
		
		$scope.login = function(m){
			_API.ajax("account/login", m).success(function(json){
				$scope.$emit('login', json);
			}).error(function(json){
				alert(json.msg);
			});
		}
		
	}]);
	
	app.action(['/autoLogin'], 'account/autoLogin.html', 
		['$scope', '$route', '_API', '$location', '$timeout', '$rootScope', 
		function($scope, $route, _API, $location, $timeout, $rootScope) {
		//自动登录
		_API.ajax("account/info", {}).success(function(json){
			$scope.$emit('login', json);
			if(!json.peopleInfo) {
				$location.path('/login');
			}
		});
	}]);
	
})($SDC.app);
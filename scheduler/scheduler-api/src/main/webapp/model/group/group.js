$SDC.route('group/', function(app) {

	app.action(['/group/page/:page', '/group/search/:searchWord/page/:page'], 'groupPage.html', ['$scope', '$route', '_API', '$location', '$timeout', '$rootScope',
		function($scope, $route, _API, $location, $timeout, $rootScope) {
			var type = $route.current.params.type;
			var page = $route.current.params.page;
			$scope.searchWord = $route.current.params.searchWord;
			$scope.pageUrl = '/group/page/';
			
			var reload = function(){
				_API.comm('manage/group/page', {
					role : type,
					currentpage : page,
					pagesize : 20
				}).success(function(json){
					$scope.pv = json.pv;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			var search = function(){
				_API.comm('manage/group/search', {
					word : $scope.searchWord,
					currentpage : 1,
					pagesize : 20
				}).success(function(json){
					$scope.pv = json.pv;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			if($scope.searchWord){
				search();
				$scope.pageUrl = '/group/search/'+ $scope.searchWord + '/page/';
			}else{
				reload();
			}
			
			$scope.changePeopleRole = function(people, role, msg){
				if(!confirm(msg)){
					return;
				}
				_API.comm('manage/group/changeRole', {
					peopleId : people.id,
					role : role
				}).success(function(json){
					people.role = role;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			$scope.search = function(word){
				$location.path('/group/search/' + word + '/page/1');
			}
  
		}
	]);
	
	app.action(['/group/add', '/group/edit/:id'], 'groupEdit.html', ['$scope', '$route', '_API', '$location', '$timeout', '$rootScope',
     	function($scope, $route, _API, $location, $timeout, $rootScope) {
			
     		var _id = $route.current.params.id;
     		if(_id){
     			$scope.title = "修改分组";
     		}else{
     			$scope.title = "新增分组";
     		}
     		$scope.m = {items : [{}]};

     		$scope.save = function(m){
     			var url = 'manage/group/add';
     			if(_id){
     				url = 'manage/group/update';
     			}
     			console.log(m.items);
     			m.items = angular.toJson(m.items);
     			_API.comm(url, m).success(function(json){
     				
     				alert("操作成功");
 					$location.path('/group/page/1');
     			}).error(function(json){
     				alert(json.msg);
     			});
     		};

     		if(_id){
     			_API.comm('manage/group/getById', {
     				id : _id
     			}).success(function(json){
     				$scope.m = json.data;
     			}).error(function(json){
     				alert(json.msg);
     			});
     		}

     	}]);
	
});
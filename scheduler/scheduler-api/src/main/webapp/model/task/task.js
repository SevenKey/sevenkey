$SDC.route('task/', function(app) {

	app.action(['/task/page/:page', '/task/search/:searchWord/page/:page'], 'taskPage.html', ['$scope', '$route', '_API', '$location', '$timeout', '$rootScope',
		function($scope, $route, _API, $location, $timeout, $rootScope) {
			var type = $route.current.params.type;
			var page = $route.current.params.page;
			$scope.searchWord = $route.current.params.searchWord;
			$scope.pageUrl = '/task/page/';
			
			var reload = function(){
				_API.comm('manage/task/page', {
					role : type,
					currentpage : page,
					pagesize : 20
				}).success(function(json){
					$scope.pv = json.pv;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			var search = function(){
				_API.comm('manage/task/search', {
					word : $scope.searchWord,
					currentpage : 1,
					pagesize : 20
				}).success(function(json){
					$scope.pv = json.pv;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			if($scope.searchWord){
				search();
				$scope.pageUrl = '/task/search/'+ $scope.searchWord + '/page/';
			}else{
				reload();
			}
			
			$scope.changePeopleRole = function(people, role, msg){
				if(!confirm(msg)){
					return;
				}
				_API.comm('manage/task/changeRole', {
					peopleId : people.id,
					role : role
				}).success(function(json){
					people.role = role;
				}).error(function(json){
					alert(json.msg);
				});
			};
			
			$scope.search = function(word){
				$location.path('/task/search/' + word + '/page/1');
			}
  
		}
	]);
	
	
	
});
(function(window, config){
	
	var app = angular.module('app.directive', []);
	app.directive('butterbar', ['$rootScope', '$location', function($rootScope, $location) {
    	  return {
    	    link: function(scope, element, attrs) {
    	      element.addClass('hide');
    	      
    	      $rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
    	      	  //未登录
  	      	      if(!_USER.peopleInfo) {
  	      	      	var isMatchUrl = function(path){
  	      	      		return newUrl.indexOf(path) + path.length == newUrl.length;
  	      	      	}
  	      	      	
  	      	      	if(isMatchUrl('/login') || isMatchUrl('/autoLogin')) {
  	      	      		//登录页面
  	      	      	} else {
  	      	      		localStorage.setItem("LAST_URL", $location.$$path);
  	      	      		//阻止事件，并跳转到登录页
				        event.preventDefault();
				        setTimeout(function(){
					        $location.path('/autoLogin');
					        $rootScope.$apply();
				        }, 1);
  	      	      	}
	    	      }
    	      });
			  
			  var count = 0, time = (new Date()).getTime();
    	      $rootScope.$on('$routeChangeStart', function(evt, next, current) {
    	      	  //网页跳转 20次以上 或 停留 5 分钟，自动刷新一次，以解决内存泄露的问题
    	      	  if(count>20 || (new Date()).getTime()-time>300000) {
    	      	  	//indow.location.reload();
    	      	  }
      	      	  //count ++ ;

    	      	  //取得路径前缀，用以作菜单
    	      	  if(next.$$route) {
	    	      	  var path = next.$$route.originalPath;
	    	      	  $rootScope._url = path;
	    	      	  $rootScope._urls = next.$$route.originalPath.split('/').slice(1);
    	      	  }else {
                      $rootScope._urls = [];
    	      	  }
    	      	  $rootScope._params = next.params;
    	      	  setTimeout(function(){
  	    	      	  $(window).trigger('resize');
    	      	  }, 20);
    	      	  
    	    	  //其它统计
    	    	  $rootScope.$broadcast('ChangeMenu', null);
    	    	  $("body").animate({scrollTop: 0}, 0); 
    	          element.removeClass('hide');
    	      });

    	      $rootScope.$on('$routeChangeSuccess', function() {
    	        element.addClass('hide');
    	      });
    	    }
    	  };
    }]);

    //分页指令
    app.directive('page', function() {
    	var page = function(currentpage, totalpage, centerCount){
    		var omit = {text: '...', canUser: false, omit: true};//省略
    		var first = {text: 1, active: false, canUse: true};
    		var last = {text: totalpage, active: false, canUse: true};
    		var o = [];
    		if(currentpage != 1) {
    			o.push(first);
    		}
    		
    		var begin = currentpage - centerCount;
    		if(begin >= 3) {//第二页是省略号
    			o.push(omit);
    		}
    		for(var i=0; i<centerCount; i++) {//当前页左边
    			if(begin+i >= 2) {
    				o.push({text: begin+i, active: false, canUse: true});
    			}
    		};
    		o.push({text: currentpage, active: true, canUse: true});//当前页
    		for(var i=0; i<centerCount; i++) {//当前页右边
    			var temp = currentpage + i + 1;
    			if(temp < totalpage) {
    				o.push({text: temp, active: false, canUse: true});
    			}
    		};
    		
    		if(currentpage + centerCount < totalpage) {
    			o.push(omit);
    		}
    		if(currentpage < totalpage) {
    			o.push(last);
    		}
    		return o;
    	};
    	
    	var link = function(scope, element, attrs, tabsCtrl) {
    		var count = scope.count;
    		if(!count) {
    			count = 3;
    		}
    		scope.back = function(page){
    			if(scope.clickBack) {
    				
    			}
    			//console.log(page);
    			//阻止事件传播
    			return false;
    		};
    		scope.$watch('pageview.currentpage', function(){
    			if(scope.pageview) {
    				scope.array = page(scope.pageview.currentpage, scope.pageview.totalpage, count);
    			}
    		});
	    };

    	var o = {
	        restrict: 'E',
	        scope: {
	        	pageview: '=pageview',
	        	count : '=count',
	        	before : '=before',
	        	after : '=after',
	        	clickBack : '=clickBack'
	        },
	        template: '<ul class="pagination">'
	        	 + ' <li ng-repeat="a in array track by $index" ng-class="{disabled: a.omit, active: a.active}">'
	        	 + '  <a ng-show="!a.omit" href="{{before}}{{a.text}}{{after}}" ng-click="back(a.text);" title="第 {{a.text}} 页">{{a.text}}</a>'
	        	 + '  <a ng-show="a.omit">{{a.text}}</a>'
	        	 + ' </li>'
	        	 + '</ul>',
	        link: link
        };
    	
    	return o;
    });
    
    app.directive('xxLinked',['$parse',function($parse){
		return {
			restrict:'A',
			link:function(scope,ele,attr){
				var fn = $parse(attr['xxLinked']);
				fn(scope);
			}
		};
	}]);
	
	app.directive('select2', function () {
	    return {
	        restrict: 'A',
	        scope: {
	            config: '=',
	            ngModel: '=',
	            select2Model: '='
	        },
	        link: function (scope, element, attrs) {
                // 初始化
                var $element = $(element).select2();

                // model - view
                scope.$watch('ngModel', function (newVal) {
                	setTimeout(function(){
	                	if(newVal) {
		                	$element.val(newVal).trigger("change");
	                	}
                	}, 100);
                }, true);
                
	        }
	    }
	});
})(window);
	
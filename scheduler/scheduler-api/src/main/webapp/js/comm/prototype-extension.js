/*删除数组指定下表的元素*/
Array.prototype.remove = function(index) {
	if (isNaN(index) || index < 0 || index >= this.length) {
		return false;
	}
	this.splice(index, 1);
}
Array.prototype.removeItem = function(item, func) {
	if (!item || this.length <= 0) {
		return false;
	}
	for (var i = 0; i < this.length; i++) {
		if (func) {
			if (func(item, this[i])) {
				this.remove(i);
			}
		}else{
			if(item==this[i]){
				this.remove(i);
			}
		}
	}
}

Date.prototype.limitDays = function(day){
	var m = this.getTime()+(day*24*3600*1000);
	return new Date(m);
}

Date.prototype.limitMonth = function(month){
	return this.limitDays(month*30);
}

Date.prototype.limitYears = function(year){
	return this.limitMonth(year*12);
}

Date.prototype.getFullDay = function(separator){
	if(!separator){
		separator = '-';
	}
	var m = this.getMonth()+1;
	return this.getFullYear()+separator+(m>9?m:0+''+m)+separator+this.getDate();
}

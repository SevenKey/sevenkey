var HistoryImgSelector = function(prefix){
		var selector = {
			select : function(callback, param){
				selector.callback = callback;
				selector.param = param ? param : {};
				$.ajax({
					url : $SDC.apiDomain + '/manage/thirdapi/qiniu/list.json',
					type : 'post',
					dataType : 'json',
					"xhrFields": {
						"withCredentials": true
					},
					"crossDomain": true,
					data : {
						prefix : prefix
					},
					success : function(json){
						render(json);
					}
				});
			}
		};
		$('#img-select').click(function(e){
			var imgs = [];
			var items = $('.img-item');
			for(var i = 0; i < items.length; i++){
				if($(items[i]).hasClass('i-active')){
					imgs.push($(items[i]).attr('img-key'));
				}
			}
			$('#img-selector').modal('hide');
			if(selector.callback){
				selector.callback(imgs);
			}
		});
		
		var render = function(data){
			$('#his-img-container').html('');
			var tmpl = function(key){
				return '<span class="img-item" img-key="' + key + '">'
	              		+ '<img src="' + $SDC.imageServer + key + '?imageView2/2/w/100" />'
	              	 + '</span>';
			};
			for(var i = 0; i < data.paths.length; i++){
				$('#his-img-container').append(tmpl(data.paths[i]));
			}
			$('.img-item').click(function(e){
				if($(this).hasClass('i-active')){
					$(this).removeClass('i-active');
				}else if(selector.param['single']){
					$('.i-active').removeClass('i-active');
					$(this).addClass('i-active');
				}else{
					$(this).addClass('i-active');
				}
			});
		};
		return selector;
	};
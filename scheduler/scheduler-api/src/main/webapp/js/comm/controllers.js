(function(){
	//----------------angular
    var app = angular.module('app', ['ngRoute', 'app.service', 'app.directive', 'ngSanitize']);
    $SDC.app = app;
    
    app.action = function(pathArray, tmpl, other){
    	var param = other;
    	if(!(param instanceof Array)) {
    		param = ['$scope', '$route', '_API', '$location', '$timeout', other];
    	}
    	if(!(pathArray instanceof Array)) {
    		pathArray = [pathArray];
    	}
    	if(!app.baseTmpl) {
    		app.baseTmpl = '';
    	}
    	
    	var tmplUrl = "model/" + app.baseTmpl + tmpl + "?version=" + $SDC.version;
    	for(var i=0; i<pathArray.length; i++) {
    		(function(path, ctrl, tmplUrl){
    			app.controller(path, param);
    			app.config(['$routeProvider', function($routeProvider){
    				$routeProvider.when(path, {
    					controller : ctrl,
    					templateUrl : tmplUrl
    				});
    			}]);
    		})(pathArray[i], pathArray[0], tmplUrl);
    	}
    };
    
    app.config(['$routeProvider', '$sceProvider', '$locationProvider', function($routeProvider, $sceProvider, $locationProvider){
        $routeProvider.otherwise({redirectTo: '/error'});
        $locationProvider.html5Mode(false);
        $sceProvider.enabled(false);
    }]);
    
    app.action("/error", "error.html", function(){
    	
    });
    
    app.action("/noPermission", "noPermission.html", function(){
    	
    });
    
    
    app.action("/waiting", "waiting.html", ['$location', function($location){
    	$location.path(localStorage.getItem("toPath"));
    }]);
    
    $SDC.route = function(baseTmpl, back){
    	app.baseTmpl = baseTmpl;
    	back(app);
    	app.baseTmpl = "";
    };

})();

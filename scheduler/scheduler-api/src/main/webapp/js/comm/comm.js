(function(){
	$SDC.reloadToPath = function($location, path){
		if(!path) {
			path = window.location.hash.substr(1);
		}
		localStorage.setItem("toPath", path);
		$location.path('/waiting');
	};
})();


//基本控制
var baseController = ['$scope', '_API', '$location', '$rootScope', function($scope, _API, $location, $rootScope){
	$rootScope.$SDC = $SDC;
	$rootScope._USER = _USER;
	
	var i = 0;
	$scope.load = function(path){
		i++;
		if(i == 3) {
			$.AdminLTE.sdcInit();
		}
	}
    	
	//-------事件监听
	$scope.topBarMenu = 'index';
	$scope.$on('ChangeMenu', function(e, m) {
		$scope.topBarMenu = m;
	});
	
	
	
		
	//--------帐号-----------
	(function(){
		_API.commWhen(1, function(json){
			$location.path('/login');
		});
		_API.commWhen(-3, function(json){
			$location.path('/noPermission');
		});
		
		var login = function(json){
			_USER.peopleInfo = json.peopleInfo;
			_USER.schools = json.schools;
		}
		$scope.$on('login', function(e, json) {
			console.log(json);
			if(json.peopleInfo.role != 'ADMIN') {
				alert("无权限，请与管理员联系");
				return ;
			}
			login(json);
			
			if(json.peopleInfo) {
				var url = localStorage.getItem("LAST_URL");
				localStorage.removeItem("LAST_URL");
				$location.path(url ? url : "/");
			}
		});
//		_API.ajax("account/info", {}).success(login);//强制加载登录
		
		$scope.logout = function(){
			_API.ajax("account/logout", {}).success(function(){
				$rootScope.peopleInfo = null;
				$location.path('/login');
			}).error(function(json){
				alert(json.msgInfo);
			});
		}
	})();
	
//	$('#wrapper').hide();
//	$('#model').modal({
//	  backdrop: false,
//	  keyboard: false
//	});
}];


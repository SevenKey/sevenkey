package com.popreal.scheduler.bae;

public class BAEConfig {
	private String ak;
	private String sk;
	private String redisDbname;
	private String mysqlDbname;
	private String mysqlUrl;
	private String mysqlClassName;
	
	
	public String getAk() {
		return ak;
	}
	public void setAk(String ak) {
		this.ak = ak;
	}
	public String getSk() {
		return sk;
	}
	public void setSk(String sk) {
		this.sk = sk;
	}
	public String getRedisDbname() {
		return redisDbname;
	}
	public void setRedisDbname(String redisDbname) {
		this.redisDbname = redisDbname;
	}
	public String getMysqlDbname() {
		return mysqlDbname;
	}
	public void setMysqlDbname(String mysqlDbname) {
		this.mysqlDbname = mysqlDbname;
	}
	public String getMysqlUrl() {
		return mysqlUrl;
	}
	public void setMysqlUrl(String mysqlUrl) {
		this.mysqlUrl = mysqlUrl;
	}
	public String getMysqlClassName() {
		return mysqlClassName;
	}
	public void setMysqlClassName(String mysqlClassName) {
		this.mysqlClassName = mysqlClassName;
	}
	
}

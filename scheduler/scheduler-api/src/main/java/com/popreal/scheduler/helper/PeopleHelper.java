package com.popreal.scheduler.helper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import net.vsame.url2sql.helper.SqlHelper;
import net.vsame.url2sql.helper.Url2SqlContext;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.sql.Model;

import com.popreal.scheduler.util.EncryptionUtils;

public class PeopleHelper {

	private static final String SESSION_PEOPLE = "people";
	private static final int maxTime = 60 * 60 * 24 * 30;//一个月
	private static final String COOKIEKEY = "jtdq";
	private static final String REG_SECRET_KEY = "gaollg";
	
	private static Url2SqlContext getContext(){
		return WebHelper.getContext();
	}
	/**
	 * 获取登录用户信息
	 * @return null表示未登录
	 */
	public static Model getPeopleInfo(){
		Object u = getContext().getSessionVal(SESSION_PEOPLE);
		if(u != null) {
			return (Model) u;
		}
		//偿试从Cookie登录
		Model m = PeopleHelper.getPeopleInfo4Cookie();
		if(m != null) {
			return m;
		}
		return PeopleHelper.getPeopleInfo4Param();
	}
	
	public static Model getPeopleInfo4Cookie(){
		Cookie[] cookies = getContext().getRequest().getCookies();
		if(cookies == null){
			return null;
		}
		String key = null;
		for(Cookie c : cookies) {
			if(COOKIEKEY.equals(c.getName())) {
				key = c.getValue();
			}
		}
		return getPeopleInfo4CookieKey(key);
	}
	
	public static Model getPeopleInfo4Param(){
		String jsesson = getContext().getParam("...JSESSIONID");
		if(jsesson == null) {
			return null;
		}
		return getPeopleInfo4CookieKey(jsesson);
	}
	
	/**
	 * 从Cookie中取得用户
	 * @param str
	 * @return
	 */
	private static Model getPeopleInfo4CookieKey(String str){
		Model m = getPeopleModel(str, REG_SECRET_KEY);
		if(m != null){
			saveSessionOnley(m);//保存至session
			return m;
		}
		return null;
	}
	
	
	public static void saveSessionOnley(Model m){
		if(m != null) {
			m.remove("password");//移除密码
		}
		Url2SqlContext context = getContext();
		context.getServletSession().setAttribute(SESSION_PEOPLE, m);
	}
	
	/**
	 * 根据加密串取得用户Model
	 * @param str
	 * @param secretKey
	 * @return
	 */
	public static Model getPeopleModel(String str, String secretKey){
		if(str == null || str.length() !=82) {
			return null;
		}
		String id = str.substring(0, 36);
		String datetime = str.substring(36, 50);//36+14
		//String md5 = str.substring(50);
		//判断是否过期...
		Model m = SqlHelper.queryOne("SELECT * FROM `people_t` where id=? and removed='N'", id);
		if(m == null){
			return m;
		}
		//查找数据库密码, 重新生成一次
		String key = encryption(m, datetime, secretKey);
		if(key.equalsIgnoreCase(str)){
			return m;//说明正确
		}
		return null;
	}
	
	public static String encryption(Model m, String datetime){
		return encryption(m, datetime, REG_SECRET_KEY);
	}
	
	public static String encryption(Model m, String datetime, String secretKey){
		return encryption(m.get("id")+"", m.get("password")+"", datetime, secretKey);
	}
	
	public static String encryption(String id, String password, String datetime, String key){
		StringBuffer sb = new StringBuffer();
		sb.append(id);
		sb.append(datetime);
		sb.append(EncryptionUtils.md5(id + datetime + key + password));
		return sb.toString();
	}
	
	public static String getIpAddr() {
		HttpServletRequest request = getContext().getRequest();
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	
}

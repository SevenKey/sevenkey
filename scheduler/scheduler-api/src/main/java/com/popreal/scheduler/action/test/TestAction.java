package com.popreal.scheduler.action.test;

import java.util.List;

import net.vsame.url2sql.action.BaseAction;
import net.vsame.url2sql.helper.SqlHelper;
import net.vsame.url2sql.sql.Model;

public class TestAction extends BaseAction {
	public void test() {
		// 假设查询条件已经给定。
		context.putParam("role", "ADMIN");
		List<Model> models = SqlHelper.query("$test");
		if (models != null && models.size() != 0) {
			System.out.println(models.toString());
		}
	}
}

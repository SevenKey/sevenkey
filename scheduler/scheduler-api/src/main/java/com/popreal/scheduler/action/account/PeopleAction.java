package com.popreal.scheduler.action.account;

import java.util.Date;

import javax.servlet.http.HttpSession;

import net.vsame.url2sql.action.BaseAction;
import net.vsame.url2sql.helper.SqlHelper;
import net.vsame.url2sql.helper.WebHelper;
import net.vsame.url2sql.sql.Model;

import org.springframework.stereotype.Service;

import com.popreal.scheduler.helper.PeopleHelper;

@Service
public class PeopleAction extends BaseAction {

	public void login() {

		Model m = SqlHelper.queryOne("sql");
		if (m == null || m.size() == 0) {
			context.putError(1001, "Account or password is wrong!");
			return;
		}

		SqlHelper.execute("$updateLoginIpTime", PeopleHelper.getIpAddr(), new Date(), m.getString("id"));
		
		HttpSession session=WebHelper.getContext().getRequest().getSession(); 
		session.setAttribute("user", m);
		context.put("peopleInfo", m);
		
	}

	public void info() {
		HttpSession session=WebHelper.getContext().getRequest().getSession(); 
		Model m = (Model) session.getAttribute("user");
		
		context.put("peopleInfo", m);
		
		
	}
}

package com.popreal.scheduler.service;

import com.alibaba.fastjson.JSONObject;

public interface ExecutorService {

	/**
	 * 执行任务
	 * @param o
	 * @param config 
	 * @return 返回任务结果
	 */
	public String execute(JSONObject o, JSONObject config);
	
}

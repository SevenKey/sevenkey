package com.popreal.scheduler.action.people;

import net.vsame.url2sql.action.BaseAction;
import net.vsame.url2sql.helper.SqlHelper;
import net.vsame.url2sql.sql.Model;
import net.vsame.url2sql.utils.PageView;

public class PeopleAction extends BaseAction {

	
	public void page(){
		
		PageView pv = SqlHelper.page("$selectAll");
		
		context.put("pv", pv);
	}
	
	public void search(){
		String word = context.getParam("word");
		context.put("pv", SqlHelper.page("select", "%" + word + "%", "%" + word + "%"));
	}
	
	public void getById(){
		Model feature = SqlHelper.queryOne("$getByid");
		context.put("data", feature);
	}
	
}

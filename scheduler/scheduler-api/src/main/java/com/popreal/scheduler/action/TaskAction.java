package com.popreal.scheduler.action;

import java.util.List;

import net.vsame.url2sql.action.BaseAction;
import net.vsame.url2sql.helper.SqlHelper;
import net.vsame.url2sql.sql.Model;
import redis.clients.jedis.Jedis;

import com.alibaba.fastjson.JSON;
import com.popreal.scheduler.MainExecutor;
import com.popreal.scheduler.helper.BAERedisHelper;
import com.popreal.shop.helper.RedisHelper.RedisCallback;

public class TaskAction extends BaseAction {

	public void restartAll(){
		final List<Model> list = SqlHelper.query("sql");
		
		BAERedisHelper.use(new RedisCallback<String>() {
			@Override
			public String execute(Jedis redis) {
				//删除队列中所有数据
				redis.zremrangeByRank(MainExecutor.KEY, 0, -1);
				for(Model m : list) {
					redis.zadd(MainExecutor.KEY, 0, JSON.toJSONString(m));
				}
				return null;
			}
		});
		
	}
	
	
	public void test(){
		context.put("a", "aaaaaa");
		String env = System.getenv("SERVER_SOFTWARE");
		context.put("bae", env);
		
		BAERedisHelper.use(new RedisCallback<String>() {
			@Override
			public String execute(Jedis redis) {
				redis.set("name", "gaollg");
				context.put("name", redis.get("name"));
				return null;
			}
		});
		
		try {
			context.put("sql", SqlHelper.query("SELECT * FROM `tast_t`"));
		}catch(Exception e) {
			e.printStackTrace();
			context.put("mysqlError", e.getMessage());
		}
	}
	
}

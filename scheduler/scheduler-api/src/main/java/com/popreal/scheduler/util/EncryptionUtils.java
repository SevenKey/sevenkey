package com.popreal.scheduler.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;

/**
 * 加密工具类
 * @author golay.gao
 *
 */
public class EncryptionUtils {
	
	public final static String md5(byte[] bytes) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			// 获得MD5摘要算法的 MessageDigest 对象
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			// 使用指定的字节更新摘要
			mdInst.update(bytes);
			// 获得密文
			byte[] md = mdInst.digest();
			// 把密文转换成十六进制的字符串形式
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte byte0 = md[i];
				str[k++] = hexDigits[byte0 >>> 4 & 0xf];
				str[k++] = hexDigits[byte0 & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public final static String md5(String str, String charset) {
		return md5(str.getBytes(Charset.forName(charset)));
	}
	
	public final static String md5(String str) {
		return md5(str, "utf-8");
	}

	public static void main(String[] args) {
		System.out.println(EncryptionUtils.md5("20121221"));
		System.out.println(EncryptionUtils.md5("加密"));
	}
}

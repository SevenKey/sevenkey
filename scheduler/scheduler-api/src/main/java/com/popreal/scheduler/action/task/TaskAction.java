package com.popreal.scheduler.action.task;

import net.vsame.url2sql.action.BaseAction;
import net.vsame.url2sql.helper.SqlHelper;
import net.vsame.url2sql.utils.PageView;

public class TaskAction extends BaseAction {

	
	public void page(){
		
		PageView pv = SqlHelper.page("$selectAll");
		
		context.put("pv", pv);
	}
	
	public void search(){
		String word = context.getParam("word");
		context.put("pv", SqlHelper.page("select", "%" + word + "%", "%" + word + "%"));
	}
	
	
}

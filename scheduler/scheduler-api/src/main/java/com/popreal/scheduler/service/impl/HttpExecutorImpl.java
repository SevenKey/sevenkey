package com.popreal.scheduler.service.impl;

import net.vsame.url2sql.utils.HttpsUtils;

import com.alibaba.fastjson.JSONObject;
import com.popreal.scheduler.service.ExecutorService;

public class HttpExecutorImpl implements ExecutorService {

	@Override
	public String execute(JSONObject o, JSONObject config) {
		String url = config.getString("url");
		return HttpsUtils.post(url);
	}

}
